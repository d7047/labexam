package com.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.app.dao.IUserRepository;
import com.app.entities.User;

public class UserServiceImpl implements IUserService {
@Autowired
private IUserRepository userRepo;
	@Override
	public User findByNameAndPassword(String name,String password) {
		
		return userRepo.findByNameAndPassword(name,password).orElseThrow(()->new RuntimeException("Invalid Credentials"));
	}
	@Override
	public User findUserById(long userId) {
		
		return userRepo.findById(userId).orElseThrow(()->new RuntimeException("Invalid Id..."));
	}

}
