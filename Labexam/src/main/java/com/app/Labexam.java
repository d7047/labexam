package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Labexam {

	public static void main(String[] args) {
		SpringApplication.run(LabExam.class, args);
	}

}
